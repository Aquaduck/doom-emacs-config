;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Arif Ahsan"
      user-mail-address "arif.s.ahsan@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'leuven)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; (setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(setq-default enable-local-variables t)



;; This never works for some reason
;(setq org-download-image-dir "static/")
;(setq org-download-method 'directory)

(setq org-attach-id-dir "~/Documents/logseq-org-roam-wiki/hugo/content-org/.attach/")

(setq org-startup-with-inline-images t)

(setq org-image-actual-width nil)

;(setq org-preview-latex-default-process 'dvisvgm)

(setq org-attach-auto-tag nil)

(setq org-adapt-indentation t)

(setq org-roam-db-location "~/Documents/logseq-org-roam-wiki/hugo/content-org/org-roam.db")

(advice-add 'org-roam-buffer-activate :after #'(org-roam-buffer-deactivate))

;; Defining publish path
;; Adding org-roam-capture templates
(setq org-roam-directory "/home/kita/Documents/logseq-org-roam-wiki/hugo/content-org")
(setq org-roam-dailies-directory "journals/")
(setq org-roam-capture-templates
        ;; Default template
      '(("d" "default" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "${slug}"
         :head "#+title: ${title}\n#+date: %u\n"
         :unnarrowed t)
        ;;Template for documentation of my fedora system
        ("f" "fedora" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "fedora/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: fedora\n"
         :unnarrowed t)
        ;; Template for medical school notes
        ("m" "medschool" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool"
         :unnarrowed t)
        ;; Template for medical school concept notes
        ("c" "medschool concept" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool concept\n"
         :unnarrowed t)
        ("s" "medschool source" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool source\n"
         :unnarrowed t)
        ("l" "medschool learning objective" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool learning_objective\n"
         :unnarrowed t)
        ;; Template for Star Trek notes - mainly used for testing
        ("s" "startrek" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "startrek/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: startrek\n"
         :unnarrowed t)))

(after! org-roam
  :config
  (add-hook 'org-mode-hook #'nroam-setup-maybe))

(setq org-roam-graph-extra-config '(("landscape" . "false")
                                    ("dim" . "3")))

(defun org-roam--extract-tags-hugo (_file)
  "Extract tags from the current buffer's \"#+hugo_tags\" global property."
  (let* ((prop (or (cdr (assoc "HUGO_TAGS" (org-roam--extract-global-props '("HUGO_TAGS"))))
                   "")))
    (condition-case nil
        (split-string-and-unquote prop)
      (error
       (progn
         (lwarn '(org-roam) :error
                "Failed to parse tags for buffer: %s. Skipping"
                (or org-roam-file-name
                    (buffer-file-name)))
         nil)))))
(custom-set-variables
 '(org-roam-tag-sources '(prop hugo)))

;; Setting cache
(use-package! nov
  :mode ("\\.epub\\'" . nov-mode)
  :config
  (setq nov-save-place-file (concat doom-cache-dir "nov-places")))

;; Enabling export options from ox-hugo in org-export
(use-package! ox-hugo
  :ensure t            ;Auto-install the package from Melpa (optional)
  :after ox)

(defun cpb/convert-attachment-to-file ()
  "Convert [[attachment:..]] to [[file:..][file:..]]"
  (interactive)
  (let ((elem (org-element-context)))
    (if (eq (car elem) 'link)
        (let ((type (org-element-property :type elem)))
          ;; only translate attachment type links
          (when (string= type "attachment")
            ;; translate attachment path to relative filename using org-attach API
            ;; 2020-11-15: org-attach-export-link was removed, so had to rewrite
            (let* ((link-end (org-element-property :end elem))
                   (link-begin (org-element-property :begin elem))
                   ;; :path is everything after attachment:
                   (file (org-element-property :path elem))
                   ;; expand that to the full filename
                   (fullpath (org-attach-expand file))
                   ;; then make it relative to the directory of this org file
                   (current-dir (file-name-directory (or default-directory
                                                         buffer-file-name)))
                   (relpath (file-relative-name fullpath current-dir)))
              ;; delete the existing link
              (delete-region link-begin link-end)
              ;; replace with file: link and file: description
              (insert (format "[[file:%s][file:%s]]" relpath relpath))))))))

(defun kita/convert-attachment-to-file-loop ()
  (interactive)
  (goto-char (point-min))
  (catch 'search
    (while (/= (point) (point-max))
      (search-forward "attachment:_" nil (throw 'search 1)))))

;; - Sync all org-roam files
(defun my-org-hugo-org-roam-sync-all()
  ""
  (interactive)
  (dolist (fil (org-roam--list-files org-roam-directory))
    (with-current-buffer (find-file-noselect fil)
      (org-hugo-export-wim-to-md)
      (kill-buffer))))

(eval-after-load "nroam"
  '(defun nroam--insert ()
     "Insert nroam sections in the current buffer."
     (with-buffer-modified-unmodified
      (save-excursion
        (goto-char (point-max))
        (unless (bobp)
          (nroam--ensure-empty-line))
        (with-nroam-markers
          (nroam--insert-heading 1 "Backlinks")
          (nroam--do-separated-by-newlines #'funcall nroam--sections))
        (nroam--set-sections-visibility)))))

(defun org-hugo-follow-backlinks ()
  (interactive)
  (setq working-buffer (buffer-name))
  (setq working-nroam-start-marker nroam-start-marker)
  (save-excursion
    (goto-char (point-min))
    (while (not (>= (point) (marker-position working-nroam-start-marker)))
      (message "Searching for links...")
      (search-forward "[[file:" (marker-position nroam-start-marker) (message "No links found"))
      (org-open-at-point)
      (message (concat "Opening link to " (buffer-name)))
      (nroam-update)
      (message "Updating nroam")
      (org-hugo-export-wim-to-md)
      (message "Exporting to ox-hugo")
      (if (not (equal working-buffer (buffer-name)))
          (kill-buffer))
      (message "Moving to original buffer"))))
;;(advice-add 'org-hugo-export-wim-to-md-after-save :after #'org-hugo-follow-backlinks)

;(setq nroam-unlinked-show-references 1)

(defun kita/org-roam-export-all ()
  "Re-exports all org-roam files to Hugo markdown"
  (interactive)
  (dolist (f (org-roam--list-all-files))
    (with-current-buffer (find-file f)
      (kita/convert-attachment-to-file-loop)
      (nroam-update)
      (org-hugo-export-wim-to-md)
      (kill-buffer))))

(defun kita/italicize-string (arg)
  (concat  "*🔖 " arg "*"))

(eval-after-load "nroam"
  '(defun nroam-backlinks--insert-backlink-breadcrumbs (outline)
    "Insert OUTLINE if non-nil as a breadcrumbs heading."
    (when outline
      (let ((str-outline (concat "* " (kita/italicize-string(string-join outline " > ")))))
        (nroam-backlinks--insert-subtree str-outline)))))

(eval-after-load "nroam"
  '(defun nroam-backlinks--insert-backlink-content (content outline)
     "Insert CONTENT with OUTLINE as a heading if non-nil"
     (nroam-backlinks--insert-backlink-breadcrumbs outline)
     (insert (concat content "\n-----"))))

(defun set-inhibit-read-only-on ()
    (setq inhibit-read-only t)
    (revert-buffer))
(defun set-inhibit-read-only-off ()
    (setq inhibit-read-only nil))
(advice-add 'org-hugo-export-wim-to-md :before #'set-inhibit-read-only-on)
(advice-add 'org-hugo-export-wim-to-md :after #'set-inhibit-read-only-off)

(setq org-export-with-toc nil)
(defun kita/org-gfm-export-all ()
  "Re-exports all org-roam files to Git-flavored markdown"
  (interactive)
  (dolist (f (org-roam--list-all-files))
    (with-current-buffer (find-file f)
      (nroam-mode 0)
      (org-md-export-to-markdown)
      (kill-buffer))))
(defun org-export-output-file-name-modified (orig-fun extension &optional subtreep pub-dir)
  (unless pub-dir
    (setq pub-dir "exported-org-files")
    (unless (file-directory-p pub-dir)
      (make-directory pub-dir)))
  (apply orig-fun extension subtreep pub-dir nil))
(advice-add 'org-export-output-file-name :around #'org-export-output-file-name-modified)

(use-package! anki-editor
  :after org
  :bind (:map org-mode-map
         ("<f12>" . anki-editor-cloze-region-auto-incr)
         ("<f11>" . anki-editor-cloze-region-dont-incr)
         ("<f10>" . anki-editor-reset-cloze-number)
         ("<f9>"  . anki-editor-push-tree))
  :hook (org-capture-after-finalize . anki-editor-reset-cloze-number) ; Reset cloze-number after each capture
  :config
  (setq anki-editor-create-decks t ;; Allow anki-editor to create a new deck if it doesn't exist
        anki-editor-org-tags-as-anki-tags t)
  (defun anki-editor-cloze-region-auto-incr (&optional arg)
    "Cloze region without hint and increase card number."
    (interactive)
    (anki-editor-cloze-region my-anki-editor-cloze-number "")
    (setq my-anki-editor-cloze-number (1+ my-anki-editor-cloze-number))
    (forward-sexp))
  (defun anki-editor-cloze-region-dont-incr (&optional arg)
    "Cloze region without hint using the previous card number."
    (interactive)
    (anki-editor-cloze-region (1- my-anki-editor-cloze-number) "")
    (forward-sexp))
  (defun anki-editor-reset-cloze-number (&optional arg)
    "Reset cloze number to ARG or 1"
    (interactive)
    (setq my-anki-editor-cloze-number (or arg 1)))
  (defun anki-editor-push-tree ()
    "Push all notes under a tree."
    (interactive)
    (anki-editor-push-notes '(4))
    (anki-editor-reset-cloze-number))
  ;; Initialize
  (anki-editor-reset-cloze-number)
  )

(use-package! org-transclusion
  :defer
  :after org
  :init
  (map!
   :map global-map "<f12>" #'org-transclusion-add
   :leader
   :prefix "n"
   :desc "Org Transclusion Mode" "T" #'org-transclusion-mode))

(advice-remove 'org-link-search '+org--recenter-after-follow-link-a)
