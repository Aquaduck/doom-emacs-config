* Description
This is my literate configuration for doom emacs. Each section will be divided into what module or package the functions work on.
#+BEGIN_SRC elisp
;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Arif Ahsan"
      user-mail-address "arif.s.ahsan@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'leuven)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; (setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
#+END_SRC
Enable local variables so that my .dir-locals.el files are read properly
#+BEGIN_SRC elisp
(setq-default enable-local-variables t)
#+END_SRC
* Keybindings
  Here I map new keybindings
  #+BEGIN_SRC elisp
  #+END_SRC
* Modules
** Org-mode
Here I tell org-download to store files in a directory and make that directory `static/` in the current working directory
#+BEGIN_SRC elisp
;; This never works for some reason
;(setq org-download-image-dir "static/")
;(setq org-download-method 'directory)
#+END_SRC
#+BEGIN_SRC elisp
(setq org-attach-id-dir "~/Documents/logseq-org-roam-wiki/hugo/content-org/.attach/")
#+END_SRC
Always enable inline images in org buffers
#+BEGIN_SRC elisp
(setq org-startup-with-inline-images t)
#+END_SRC
Have org-mode use the width I define instead of the actual width
#+BEGIN_SRC elisp
(setq org-image-actual-width nil)
#+END_SRC
Change default latex rendering to make chemfig previews work
#+BEGIN_SRC elisp
;(setq org-preview-latex-default-process 'dvisvgm)
#+END_SRC
Set org-attach-auto-tag to nil to stop :ATTACH: from showing up
#+BEGIN_SRC elisp
(setq org-attach-auto-tag nil)
#+END_SRC
Enable org-adapt-indentation
#+BEGIN_SRC elisp
(setq org-adapt-indentation t)
#+END_SRC
Add additional capture templates to org-capture
,#+BEGIN_SRC elisp
(setq org-my-anki-file "~/org/anki.org")
(after! org-capture
  (add-to-list 'org-capture-templates
               '("a" "Anki cards"))
  (add-to-list 'org-capture-templates
               '("ab" "Anki basic"
                 entry
                 (file+headline org-my-anki-file "Dispatch Shelf")
                 "* %<%H:%M>   %^g\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Basic\n:ANKI_DECK: Medschool\n:ANKI_TAGS:\n:END:\n** Front\n%?\n** Back\n%x\n"))
  (add-to-list 'org-capture-templates
               '("ac" "Anki cloze"
                 entry
                 (file+headline org-my-anki-file "Dispatch Shelf")
                 "* %<%H:%M>   %^g\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Cloze\n:ANKI_DECK: Medschool\n:ANKI_TAGS:\n:END:\n** Text\n%x\n** Extra\n")))

(setq x-select-enable-clipboard t
      x-select-enable-primary t)
#+END_SRC
*** Org-roam
Setting directory for org-roam.db:
#+BEGIN_SRC elisp
(setq org-roam-db-location "~/Documents/logseq-org-roam-wiki/hugo/content-org/org-roam.db")
#+END_SRC
**** TODO Disabling org-roam buffer automatically opening after saving a file
     *NOTE*: currently doesn't work
     #+BEGIN_SRC elisp
(advice-add 'org-roam-buffer-activate :after #'(org-roam-buffer-deactivate))
     #+END_SRC
**** Org-capture templates
I have org-capture templates made for storing documentation about my computer system and my medical school notes. I have a star-trek section to test my capture templates and a default "null" capture node to fall-back on in case I currently don't have any section that a note would fit under.
#+BEGIN_SRC elisp
;; Defining publish path
;; Adding org-roam-capture templates
(setq org-roam-directory "/home/kita/Documents/logseq-org-roam-wiki/hugo/content-org")
(setq org-roam-dailies-directory "journals/")
(setq org-roam-capture-templates
        ;; Default template
      '(("d" "default" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "${slug}"
         :head "#+title: ${title}\n#+date: %u\n"
         :unnarrowed t)
        ;;Template for documentation of my fedora system
        ("f" "fedora" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "fedora/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: fedora\n"
         :unnarrowed t)
        ;; Template for medical school notes
        ("m" "medschool" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool"
         :unnarrowed t)
        ;; Template for medical school concept notes
        ("c" "medschool concept" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool concept\n"
         :unnarrowed t)
        ("s" "medschool source" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool source\n"
         :unnarrowed t)
        ("l" "medschool learning objective" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "medschool/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: medschool learning_objective\n"
         :unnarrowed t)
        ;; Template for Star Trek notes - mainly used for testing
        ("s" "startrek" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "startrek/${slug}"
         :head "#+title: ${title}\n#+date: %u\n#+hugo_tags: startrek\n"
         :unnarrowed t)))
#+END_SRC
**** Org-roam-server
Here I set up org-roam-server to visualize my org-roam database in a local web-server
;;#+BEGIN_SRC elisp
;; Enabling Org-roam's protocol extensions
(require 'org-roam-protocol)
;; Defining variables for org-roam-server
(setq org-roam-server-host "127.0.0.1"
        org-roam-server-port 8080
        org-roam-server-authenticate nil
        org-roam-server-export-inline-images t
        org-roam-server-serve-files t
        org-roam-server-served-file-extensions '("pdf" "mp4" "ogv")
        org-roam-server-network-poll t
        org-roam-server-network-arrows "from"
        org-roam-server-network-label-truncate t
        org-roam-server-network-label-truncate-length 60
        org-roam-server-network-label-wrap-length 20
        org-roam-server-default-include-filters "[{ \"id\": \"medschool\", \"parent\" : \"tags\" },{ \"id\": \"concept\", \"parent\" : \"tags\"}]"
        org-roam-server-default-exclude-filters "[{ \"id\": \"jumpstart\", \"parent\" : \"tags\" }]")
;; Start server when emacs is running
(org-roam-server-mode 1)
#+END_SRC
**** nroam initialization
/nroam/ changes the behavior of the org-roam buffer so that it is at the bottom of a buffer and looks more like it is in LogSeq
I finish the initialization here by enabling nroam-mode in org-roam buffers by default:
#+BEGIN_SRC elisp
(after! org-roam
  :config
  (add-hook 'org-mode-hook #'nroam-setup-maybe))
#+END_SRC
**** Org-roam-graph options
     Change orientation of graph output to be landscape
     #+BEGIN_SRC elisp
(setq org-roam-graph-extra-config '(("landscape" . "false")
                                    ("dim" . "3")))
     #+END_SRC
**** Making Org-roam work with Hugo's tag system
     #+BEGIN_SRC elisp
(defun org-roam--extract-tags-hugo (_file)
  "Extract tags from the current buffer's \"#+hugo_tags\" global property."
  (let* ((prop (or (cdr (assoc "HUGO_TAGS" (org-roam--extract-global-props '("HUGO_TAGS"))))
                   "")))
    (condition-case nil
        (split-string-and-unquote prop)
      (error
       (progn
         (lwarn '(org-roam) :error
                "Failed to parse tags for buffer: %s. Skipping"
                (or org-roam-file-name
                    (buffer-file-name)))
         nil)))))
(custom-set-variables
 '(org-roam-tag-sources '(prop hugo)))
     #+END_SRC
*** Nov.el
Here I set up nov.el to read epub files. I don't use nov.el normally, but it is good to have as I may use emacs to annotate epubs in the future.
#+BEGIN_SRC elisp
;; Setting cache
(use-package! nov
  :mode ("\\.epub\\'" . nov-mode)
  :config
  (setq nov-save-place-file (concat doom-cache-dir "nov-places")))
#+END_SRC
*** Ox-hugo
Ox-hugo allows me to export my org-roam directory into hugo-compatible markdown files and directory structure. I use this to host my org-roam directory as a static website that I can browse from anywhere.
#+BEGIN_SRC elisp
;; Enabling export options from ox-hugo in org-export
(use-package! ox-hugo
  :ensure t            ;Auto-install the package from Melpa (optional)
  :after ox)
#+END_SRC
The following scripts are adapted from those written by Sidharth Arya.
Here I create a function that processes org-roam tags into ox-hugo tags.
;;#+BEGIN_SRC elisp
(defun org-hugo--tag-processing-fn-roam-tags(tag-list info)
  "Process org roam tags for org hugo"
  (if (org-roam--org-roam-file-p)
      (append tag-list
              (mapcar #'downcase (org-roam--extract-tags)))
    (mapcar #'downcase tag-list)
    ))
(add-to-list 'org-hugo-tag-processing-functions 'org-hugo--tag-processing-fn-roam-tags)
;;#+END_SRC
I found an elisp function someone wrote to convert attachment: links into file: links for ox-hugo exports. The link is here: https://vxlabs.com/2020/07/25/emacs-lisp-function-convert-attachment-to-file/
#+BEGIN_SRC elisp
(defun cpb/convert-attachment-to-file ()
  "Convert [[attachment:..]] to [[file:..][file:..]]"
  (interactive)
  (let ((elem (org-element-context)))
    (if (eq (car elem) 'link)
        (let ((type (org-element-property :type elem)))
          ;; only translate attachment type links
          (when (string= type "attachment")
            ;; translate attachment path to relative filename using org-attach API
            ;; 2020-11-15: org-attach-export-link was removed, so had to rewrite
            (let* ((link-end (org-element-property :end elem))
                   (link-begin (org-element-property :begin elem))
                   ;; :path is everything after attachment:
                   (file (org-element-property :path elem))
                   ;; expand that to the full filename
                   (fullpath (org-attach-expand file))
                   ;; then make it relative to the directory of this org file
                   (current-dir (file-name-directory (or default-directory
                                                         buffer-file-name)))
                   (relpath (file-relative-name fullpath current-dir)))
              ;; delete the existing link
              (delete-region link-begin link-end)
              ;; replace with file: link and file: description
              (insert (format "[[file:%s][file:%s]]" relpath relpath))))))))
#+END_SRC
**** Sidharth Programs
These are the scripts from Sidharth Arya that I am currently replacing

Loops through a buffer and changes all attachments to file format.
#+BEGIN_SRC elisp
(defun kita/convert-attachment-to-file-loop ()
  (interactive)
  (goto-char (point-min))
  (catch 'search
    (while (/= (point) (point-max))
      (search-forward "attachment:_" nil (throw 'search 1)))))
#+END_SRC


I define a function to process backlinks and attach them at the end of each .org file before exporting to markdown.
;;#+BEGIN_SRC elisp
(defun org-hugo--org-roam-backlinks (backend)
  (when (equal backend 'hugo)
  (when (org-roam--org-roam-file-p)
    (goto-char (point-min))
    (replace-string "{" "")
    (goto-char (point-min))
    (replace-string "}" "")
    (goto-char (point-max))
    (org-roam-buffer--insert-backlinks))))
(add-hook 'org-export-before-processing-hook #'org-hugo--org-roam-backlinks)
#+END_SRC
This function triggers ox-hugo whenever I save a file
*NOTE* May be implemented in ox-hugo directly via org-hugo-auto-export-mode
;;#+BEGIN_SRC elisp
(defun org-hugo--org-roam-save-buffer(&optional no-trace-links)
  "On save export to hugo"
  (when (org-roam--org-roam-file-p)
      (org-hugo-export-wim-to-md)))
(add-to-list 'after-save-hook #'org-hugo--org-roam-save-buffer)
Finally, I have a function that syncs all my org-roam files to be exported through rapidly cycling the buffers of all the files
*NOTE* This function appears to break backlink exports as pages have the wrong backlinks
#+BEGIN_SRC elisp
;; - Sync all org-roam files
(defun my-org-hugo-org-roam-sync-all()
  ""
  (interactive)
  (dolist (fil (org-roam--list-files org-roam-directory))
    (with-current-buffer (find-file-noselect fil)
      (org-hugo-export-wim-to-md)
      (kill-buffer))))
#+END_SRC
**** My export functions to work with nroam
Replacing nroam's function to remove the :noexport: tag, as I want backlinks included in the export
#+BEGIN_SRC elisp
(eval-after-load "nroam"
  '(defun nroam--insert ()
     "Insert nroam sections in the current buffer."
     (with-buffer-modified-unmodified
      (save-excursion
        (goto-char (point-max))
        (unless (bobp)
          (nroam--ensure-empty-line))
        (with-nroam-markers
          (nroam--insert-heading 1 "Backlinks")
          (nroam--do-separated-by-newlines #'funcall nroam--sections))
        (nroam--set-sections-visibility)))))
#+END_SRC
When ox-hugo exports, follow links in current buffer to also update the backlinks for the linked buffers. Temporarily set to manual because it breaks in certain large files.
#+BEGIN_SRC elisp
(defun org-hugo-follow-backlinks ()
  (interactive)
  (setq working-buffer (buffer-name))
  (setq working-nroam-start-marker nroam-start-marker)
  (save-excursion
    (goto-char (point-min))
    (while (not (>= (point) (marker-position working-nroam-start-marker)))
      (message "Searching for links...")
      (search-forward "[[file:" (marker-position nroam-start-marker) (message "No links found"))
      (org-open-at-point)
      (message (concat "Opening link to " (buffer-name)))
      (nroam-update)
      (message "Updating nroam")
      (org-hugo-export-wim-to-md)
      (message "Exporting to ox-hugo")
      (if (not (equal working-buffer (buffer-name)))
          (kill-buffer))
      (message "Moving to original buffer"))))
;;(advice-add 'org-hugo-export-wim-to-md-after-save :after #'org-hugo-follow-backlinks)
#+END_SRC

Show unlinked references by default
#+BEGIN_SRC elisp
;(setq nroam-unlinked-show-references 1)
#+END_SRC

Function to export all org-roam files to Hugo markdown - adapted from jethrokuan's setup
#+BEGIN_SRC elisp
(defun kita/org-roam-export-all ()
  "Re-exports all org-roam files to Hugo markdown"
  (interactive)
  (dolist (f (org-roam--list-all-files))
    (with-current-buffer (find-file f)
      (kita/convert-attachment-to-file-loop)
      (nroam-update)
      (org-hugo-export-wim-to-md)
      (kill-buffer))))
#+END_SRC

Function to italicize a string based on org formatting
#+BEGIN_SRC elisp
(defun kita/italicize-string (arg)
  (concat  "*🔖 " arg "*"))
#+END_SRC

Rewrite nroam function to italicize output
#+BEGIN_SRC elisp
(eval-after-load "nroam"
  '(defun nroam-backlinks--insert-backlink-breadcrumbs (outline)
    "Insert OUTLINE if non-nil as a breadcrumbs heading."
    (when outline
      (let ((str-outline (concat "* " (kita/italicize-string(string-join outline " > ")))))
        (nroam-backlinks--insert-subtree str-outline)))))
#+END_SRC

Rewrite nroam function to add horizontal line after backlink preview
#+BEGIN_SRC elisp
(eval-after-load "nroam"
  '(defun nroam-backlinks--insert-backlink-content (content outline)
     "Insert CONTENT with OUTLINE as a heading if non-nil"
     (nroam-backlinks--insert-backlink-breadcrumbs outline)
     (insert (concat content "\n-----"))))
#+END_SRC

Adding advice to org-hugo-export-wim-to-md to temporarily inhibit read-only text
#+BEGIN_SRC elisp
(defun set-inhibit-read-only-on ()
    (setq inhibit-read-only t)
    (revert-buffer))
(defun set-inhibit-read-only-off ()
    (setq inhibit-read-only nil))
(advice-add 'org-hugo-export-wim-to-md :before #'set-inhibit-read-only-on)
(advice-add 'org-hugo-export-wim-to-md :after #'set-inhibit-read-only-off)
#+END_SRC
*** Ox-gfm
Ox-gfm is a tool to export org files to github-flavored markdown. I use this in an attempt to export to obsidian.
#+BEGIN_SRC elisp
(setq org-export-with-toc nil)
(defun kita/org-gfm-export-all ()
  "Re-exports all org-roam files to Git-flavored markdown"
  (interactive)
  (dolist (f (org-roam--list-all-files))
    (with-current-buffer (find-file f)
      (nroam-mode 0)
      (org-md-export-to-markdown)
      (kill-buffer))))
(defun org-export-output-file-name-modified (orig-fun extension &optional subtreep pub-dir)
  (unless pub-dir
    (setq pub-dir "exported-org-files")
    (unless (file-directory-p pub-dir)
      (make-directory pub-dir)))
  (apply orig-fun extension subtreep pub-dir nil))
(advice-add 'org-export-output-file-name :around #'org-export-output-file-name-modified)
#+END_SRC
*** anki-editor
    anki-editor allows me to create new anki cards right from emacs
    #+BEGIN_SRC elisp
(use-package! anki-editor
  :after org
  :bind (:map org-mode-map
         ("<f12>" . anki-editor-cloze-region-auto-incr)
         ("<f11>" . anki-editor-cloze-region-dont-incr)
         ("<f10>" . anki-editor-reset-cloze-number)
         ("<f9>"  . anki-editor-push-tree))
  :hook (org-capture-after-finalize . anki-editor-reset-cloze-number) ; Reset cloze-number after each capture
  :config
  (setq anki-editor-create-decks t ;; Allow anki-editor to create a new deck if it doesn't exist
        anki-editor-org-tags-as-anki-tags t)
  (defun anki-editor-cloze-region-auto-incr (&optional arg)
    "Cloze region without hint and increase card number."
    (interactive)
    (anki-editor-cloze-region my-anki-editor-cloze-number "")
    (setq my-anki-editor-cloze-number (1+ my-anki-editor-cloze-number))
    (forward-sexp))
  (defun anki-editor-cloze-region-dont-incr (&optional arg)
    "Cloze region without hint using the previous card number."
    (interactive)
    (anki-editor-cloze-region (1- my-anki-editor-cloze-number) "")
    (forward-sexp))
  (defun anki-editor-reset-cloze-number (&optional arg)
    "Reset cloze number to ARG or 1"
    (interactive)
    (setq my-anki-editor-cloze-number (or arg 1)))
  (defun anki-editor-push-tree ()
    "Push all notes under a tree."
    (interactive)
    (anki-editor-push-notes '(4))
    (anki-editor-reset-cloze-number))
  ;; Initialize
  (anki-editor-reset-cloze-number)
  )
    #+END_SRC
*** Org-transclusion
#+BEGIN_SRC elisp
(use-package! org-transclusion
  :defer
  :after org
  :init
  (map!
   :map global-map "<f12>" #'org-transclusion-add
   :leader
   :prefix "n"
   :desc "Org Transclusion Mode" "T" #'org-transclusion-mode))
#+END_SRC
Preventing "recentering" error:
#+BEGIN_SRC elisp
(advice-remove 'org-link-search '+org--recenter-after-follow-link-a)
#+END_SRC
